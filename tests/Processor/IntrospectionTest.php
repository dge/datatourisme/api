<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Tests\Datatourisme\Api\Processor;

class IntrospectionTest extends AbstractGraphQLTest
{
    public function testIntrospectionType()
    {
        $res = $this->queryGraqhQL('
        {
            __type(name: "PointOfInterest") {
                name
            }
        }');
        $this->assertEquals($res, '{"data":{"__type":{"name":"PointOfInterest"}}}');
    }
}
