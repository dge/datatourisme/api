<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Tests\Datatourisme\Api\Processor\Filter;

use Tests\Datatourisme\Api\Processor\AbstractGraphQLTest;

class GeoSpatialTest extends AbstractGraphQLTest
{
    public function testGeoBounding()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                    { isLocatedAt: {schema_geo: { _geo_bounding: {sw_lng: "2.55" , sw_lat:"44.33" , ne_lng: "2.58" , ne_lat:"44.36" } }}}
                ]
            )
            {
                total
            }
        }
        ');
        $res = json_decode($res, true);
        $this->assertEquals(84, $res['data']['poi']['total']);
    }

    public function testGeoDistance()
    {
        $res = $this->queryGraqhQL('
            {
                poi(
                    filters: [
                        { isLocatedAt: {schema_geo: { _geo_distance: {lng: "2.57" , lat: "44.34" , distance: "1" } }}}
                    ]
                )
                {
                    total
                }
            }
        ');
        $res = json_decode($res, true);
        $this->assertEquals(6, $res['data']['poi']['total']);
    }
}
