<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Tests\Datatourisme\Api\Processor\Filter;

use Tests\Datatourisme\Api\Processor\AbstractGraphQLTest;

class LogicalTest extends AbstractGraphQLTest
{
    public function testOr()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                size: 1,
                filters: [
                    { _or : [
                            {hasDescription: {shortDescription: {_text: "trop"}}},
                            {rdf_type: {_eq: "http://test"}}
                        ]
                    }
                ]
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"dc_identifier":["TFO299667308000"]}]}}}', $res);
    }

    public function testAnd()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                    { _and : [
                            {hasDescription: {shortDescription: {_text: "trop"}}},
                            {rdf_type: {_eq: "http://test"}}
                        ]
                    }
                ]
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }
        ');
        $this->assertEquals('{"data":{"poi":{"total":0,"results":[]}}}', $res);
    }

    public function testAnd2()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                    {
                        allowedPersons: {
                            _and: [
                                {_gt: "29"},
                                {_lt: "31"}
                            ]
                        }
                    }
                ]
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }
        ');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"dc_identifier":["TFO308495934640"]}]}}}', $res);
    }

    public function testNot()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                    {_not : [
                            {allowedPersons: {_lt: "30"}},
                            {allowedPersons: {_gt: "30"}}
                        ]
                    },
                    {allowedPersons: {_gt: "0"}}
                ]
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"dc_identifier":["TFO308495934640"]}]}}}', $res);
    }
}
