<?php

namespace Tests\Datatourisme\Api\Schema;

use Datatourisme\Api\Schema\AbstractSchema;
use Datatourisme\Api\Schema\Compiler\SchemaCompiler;
use Datatourisme\Api\Schema\SchemaLoader;
use PHPUnit\Framework\TestCase;

class SchemaLoaderTest extends TestCase
{
    public function testCompile()
    {
        $compiler = new SchemaCompiler();
        $loader = new SchemaLoader($compiler);
        $loader->setCache(false);
        $schema = $loader->load(__DIR__.'/../../schema/datatourisme.yml');
        $this->assertTrue($schema instanceof AbstractSchema);
    }
}
