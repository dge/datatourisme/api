# Changelog
Tous les changements notables apportés à ce projet seront documentés dans ce fichier.

Le format est basé sur [Keep a Changelog](http://keepachangelog.com/fr/1.0.0/)
et ce projet adhère à [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [2.4.5] - 2020-10-15

### Mise à jour
- Mise à jour de la librairie EasyRDF

## [2.4.4] - 2020-10-15

### Mise à jour
- Utilisation de l'ontologie DATAtourisme v2.4.4

## [2.4.1] - 2020-06-12

### Mise à jour
- Utilisation de l'ontologie DATAtourisme v2.4.1

## [2.3.7] - 2020-04-29

### Correction
- Correction de la référence à `youshido/graphql` dans `composer.json`

## [2.3.6] - 2020-01-28

### Modifications
- Utilisation de l'ontologie DATAtourisme v2.3.6

## [2.3.5] - 2019-12-20

### Correction
- Correction des versions de certaines dépendances

## [2.3.4] - 2019-12-13

### Modifications
- Utilisation de l'ontologie DATAtourisme v2.3.4
- Ajout de la méthode setTimeout dans le SparqlResolver
- Blazegraph : Désactivation du QueryHint runtime global

## [2.1.0] - 2019-05-16

### Modifications
- Utilisation de l'ontologie DATAtourisme v2.1

### Correction
- Correction de la documentation

## [2.0.1] - 2019-02-26

### Correction
- Correction de la documentation

## [2.0.0] - 2019-01-14

### Modifications
- Utilisation de l'ontologie DATAtourisme v2.x
- Mise à jour de la documentation

## [1.1.3] - 2018-07-26

### Modifications
- Mise à jour de la documentation
- Mise à jour du schema DATAtourisme

## [1.1.2] - 2018-07-26

### Correction
- SchemaCompiler : correction de l'URI de xsd:dateTime pour le mapping des types

## [1.1.1] - 2018-06-18

### Modifications
- Composer : suppression de symfony/config
- Composer : modification de la version symfony/yaml
- Schema : rdfs:comment est maintenant rdf:langString

## [1.1.0] - 2018-06-04

### Ajouts
- Gestion des propriétés multilingues
- Ajout d'un argument lang

### Modifications
- Ajout d'un tri par défaut sur la requête SPARQL (uri ASC)
- Ajout d'un pré-traitement d'ordonnancement dans le QueryHydrator
- Ajout d'une option pour choisir le graph par défaut dans le VirtuosoResolver
- Modification du filtre uri : utilisation de VALUES à la place de BIND

## [1.0.0] - 2018-04-24

### Ajouts
- Code source initial
- Documentation





