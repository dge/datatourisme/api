# _or

**_or** réalise une opération logique **OR** sur un tableau d'expressions. 

```graphql
{
    poi(
        filters: [
            { _or : [
                    { <expression1> },
                    { <expression2> }
                ]
            }
        ]
    )
    {
        results {
            <propriété_extraite>
        }
    }
}
```


### Exemples ###

```graphql
{
    poi(
        filters: [
            { _or : [
                    {hasDescription: {shortDescription: {_text: "trop"}}},
                    {rdf_type: {_eq: "http://test"}}
                ]
            }
        ]
    )
    {
        results {
            dc_identifier
        }
    }
}
```
Cette requête extraira tous les identifiants des **POI** dont la ressource est de type "http://test" ou dont la description contient **"trop"**. Le résultat sera de ce type :
```json
{
  "data": {
    "poi": {
      "results": [
        {
          "dc_identifier": [
            "TFO299667308000"
          ]
        }
      ]
    }
  }
}
```