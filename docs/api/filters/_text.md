# _text

Permet de rechercher sur une chaine. L'opérateur **_text** extrait les POI dont la propriété visée contient la valeur fournie.

```graphql
{
    poi(
        filters: [
            { <propriété>: {_text: <valeur>} }
        ]
    )
    {
        results {
            <propriété_extraite>
        }
    }
}
```

### Exemples ###

```graphql
{
  poi(
    size: 1,
    filters: [
      {
        hasDescription: {
          shortDescription: {
            _text: "trop"
          }
        }
      }
    ]
  ) 
  {
    results{
      dc_identifier
    }
  }
}
```
Cette requête extraira tous les identifiants des **POI** dont la description contient "trop". Le résultat sera de ce type :
```json
{
  "data": {
    "poi": {
      "results": [
        {
          "dc_identifier": [
            "TFO299667308000"
          ]
        }
      ]
    }
  }
}
```