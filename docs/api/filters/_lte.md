# _lte

Détermine une condition permettant une comparaison inférieure ou égale. L'opérateur **_lte** extrait les POI dont la propriété visée est inférieure ou égale à la valeur fournie.

```graphql
{
    poi(
        filters: [
            { <propriété>: {_lte: <valeur>} }
        ]
    )
    {
        results {
            <propriété_extraite>
        }
    }
}
```

### Exemples ###

```graphql
{
  poi(
    filters: [
      {
        allowedPersons: {
          _lte: 15
        }
      }
    ]
  )
  {
    results{
      dc_identifier
    }
  }
}
```
Cette requête extraira tous les identifiants des **POI** où le nombre de personnes autorisées (**allowedPersons**) est inférieur ou égal à 30. Le résultat sera de ce type :
```json
{
  "data": {
    "poi": {
      "results": [
        {
          "dc_identifier": [
            "TFO073163031006"
          ]
        },
        {
          "dc_identifier": [
            "TFO096220245274"
          ]
        }
      ]
    }
  }
}
```