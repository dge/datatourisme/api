# Filtres

DATAtourisme permet de filtrer les résultats en utilisant des fonctions telles que `gt` *(plus grand que)*
ou `eq` *(égal)*.

Afin d'être facilement distinguables, tous les filtres sont préfixés par un underscore: `_gte`, `_not`, etc.

Les **filtres** permettent donc de sélectionner des résultats en fonction de critères passés en paramètres.

```graphql
{
    poi(
        filters:[
            { <expression1> },
            { <expression2> }
        ]
    )
    {
        # ...
    }
}
```

Les propriétés hiérarchiques peuvent être atteintes en intégrant la hiérarchie dans le filtre :

```graphql
{
    poi(
        filters:[
            { <propriété1> : { <propriété2>: { <propriété3> : <valeur> } }  }
        ]
    )
    {
        # ...
    }
}

```
Les filtres sont cumulatifs par défaut : le POI doit répondre à tous l'ensemble des conditions pour être retourné.
L'utilisateur peut utiliser les opérateurs logiques pour modifier ce comportement :

```graphql
{
    poi(
        filters: 
        { 
            _or: [
                { <expression1> },
                { <expression2> }
            ]
        }
    )
        # ...
    }
}
```

Les opérateurs logiques sont cumulatifs hiérarchiquement :

```graphql
{
    poi(
        filters: 
        { 
            _or: [
                { <expression1> },
                { 
                    _and: [
                        { <expression2> },
                        { <expression3> },
                    ]
                }
            ]
        }
    )
    {
        # ...
    }
}
```

Un filtre peut se présenter sous plusieurs formes :

```graphql
{
    poi(
        sort:[
            { allowedPersons: { order: "desc" } },
        ]
        filters: [
            { allowedPersons: {_gt: "10"} },
            { allowedPersons: {_lt: "25"} }
        ]
    )
    {
        # ...
    }
}
```

## Opérateurs de comparaison {docsify-ignore}

| Filtres       | Description                                                                     |
| ------------- |:-------------------------------------------------------------------------------|
| [_eq](api/filters/_eq.md) | La propriété est égale à la valeur                                              |
| [_gt](api/filters/_gt.md)           | La propriété est plus grande que la valeur                                      |
| [_gte](api/filters/_gte.md)          | La propriété est plus grande ou égale à la valeur                               |
| [_lt](api/filters/_lt.md)           | La propriété est plus petite que la valeur                                      |
| [_lte](api/filters/_lte.md)          | La propriété est plus petite ou égale à la valeur                                |
| [_ne](api/filters/_ne.md)           | La propriété n'est pas égale à la valeur                                        |
| [_in](api/filters/_in.md)           | La propriété est incluse dans le tableau de valeur(s)                              |
| [_nin](api/filters/_nin.md)          | La propriété n'est pas incluse dans le tableau de valeur(s)                        |
| [_text](api/filters/_text.md)         | Effectue une recherche full-text                                                |
| **Geospatial** ||
| [_geo_distance](api/filters/_geo_distance.md) | Effectue une recherche géospatiale sur la base de coordonnées et d'une distance |
| [_geo_bounding](api/filters/_geo_bounding.md) | Effectue une recherche géospatiale sur la base d'une zone délimitée             |


## Opérateurs logiques {docsify-ignore}

| Filtres       | Description                                                                     |
| ------------- |:-------------------------------------------------------------------------------|
| [_or](api/filters/_or.md)           | Les filtres sont joints par une clause logique OU                               |
| [_and](api/filters/_and.md)          | Les filtres sont joints par une clause logique ET                               |
| [_not](api/filters/_not.md)          | Le POI ne doit pas répondre au filtre pour être retourné                        |
