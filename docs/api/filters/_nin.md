# _nin

Détermine une condition de différence par rapport aux éléments d'un tableau. L'opérateur **_nin** extrait les POI dont la propriété visée est différente de toutes les valeurs fournies.

```graphql
{
    poi(
        filters: [
            { <propriété>: {_nin: [<valeur1>, <valeur2>, ...]} }
        ]
    )
    {
        results {
            <propriété_extraite>
        }
    }
}
```

### Exemples ###

```graphql
{
  poi(
    filters: [
      {
        allowedPersons: {
          _nin: [10,15,20,25,203]
        }
      }
    ]
  ) 
  {
    results{
      dc_identifier
    }
  }
}
```
Cette requête extraira tous les identifiants des **POI** où le nombre de personnes autorisées (**allowedPersons**) est différent de 10, 15, 20, 25 et 203. Le résultat sera de ce type :
```json
{
  "data": {
    "poi": {
      "results": [
        {
          "dc_identifier": [
            "TFO308495934640"
          ]
        }
      ]
    }
  }
}
```