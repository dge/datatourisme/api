<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Collection;

/**
 * Collection with unique items.
 */
class AbstractCollection extends \ArrayObject
{
    /**
     * Add the a value only if not empty value and not already exists in underlying array.
     *
     * @param $value
     *
     * @return $this
     */
    public function add($value)
    {
        if (!empty((string) $value) && !in_array($value, $this->getArrayCopy())) {
            $this->append($value);
        }

        return $this;
    }
}
