<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Collection;

class UnionCollection extends AbstractCollection
{
    /**
     * @return string
     */
    public function __toString()
    {
        $output = implode("} \nUNION {", $this->getArrayCopy());
        if ($this->count() > 1) {
            $output = '{'.$output.'}';
        }

        return $output;
    }
}
