<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Filter;

use Datatourisme\Api\Schema\Field\AbstractField;

interface ConditionalFilterInterface extends FilterInterface
{
    public function generate($subject, AbstractField $fieldDef, $value);
}
