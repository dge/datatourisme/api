<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Sparql;

class Where
{
    private $_where;

    public function __construct($where)
    {
        $this->_where = $where;
    }

    public function __toString()
    {
        return 'WHERE {'.$this->_where.'}';
    }
}
