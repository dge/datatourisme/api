<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Sparql\Functions;

class Filter
{
    private $_filter;

    public function __construct($filter)
    {
        $this->_filter = $filter;
    }

    public function __toString()
    {
        return sprintf('FILTER(%s)', $this->_filter);
    }
}
