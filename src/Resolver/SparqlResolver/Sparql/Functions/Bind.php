<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Sparql\Functions;

use Datatourisme\Api\Resolver\SparqlResolver\Sparql\Triplet;

class Bind
{
    private $_triplet;

    public function __construct($var, $value)
    {
        $this->_triplet = new Triplet($value, 'as', $var);
    }

    public function __toString()
    {
        return 'BIND('.(string) $this->_triplet.')';
    }
}
