<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Sparql;

class UpletSuite
{
    private $_tabUplet = [];
    private $_glue;

    public function __construct($tabUplet, $glue = ',')
    {
        $this->_tabUplet = $tabUplet;
        $this->_glue = $glue;
    }

    public function __toString()
    {
        return '('.implode($this->_glue, $this->_tabUplet).')';
    }
}
