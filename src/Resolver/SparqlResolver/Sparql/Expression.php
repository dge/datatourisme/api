<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Sparql;

class Expression
{
    private $_expression;

    public function __construct($expression)
    {
        $this->_expression = $expression;
    }

    public function __toString()
    {
        if ($this->_expression instanceof Uplet) {
            return (string) $this->_expression;
        }
    }
}
