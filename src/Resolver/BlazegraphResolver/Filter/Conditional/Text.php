<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\BlazegraphResolver\Filter\Conditional;

use Datatourisme\Api\Resolver\SparqlResolver\Collection\IntersectCollection;
use Datatourisme\Api\Resolver\SparqlResolver\Filter\ConditionalFilterInterface;
use Datatourisme\Api\Resolver\SparqlResolver\Utils\SparqlUtils;
use Datatourisme\Api\Schema\Field\AbstractField;

class Text implements ConditionalFilterInterface
{
    public function getName()
    {
        return '_text';
    }

    public function generate($subject, AbstractField $fieldDef, $value)
    {
        $service = new IntersectCollection();
        $service->add($subject.' <http://www.bigdata.com/rdf/search#search> '.SparqlUtils::formatLiteralValue($value, $fieldDef->getDatatype()));

        $sparql = new IntersectCollection();
        $sparql->add('SERVICE <http://www.bigdata.com/rdf/search#search> '.$service);
        $sparql->add('<http://www.bigdata.com/queryHints#SubQuery> <http://www.bigdata.com/queryHints#optimizer> "Static"');

        return $sparql;
    }
}
