<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

/**
 * GteType.php.
 */

namespace Datatourisme\Api\Schema\Field\Geo;

use Datatourisme\Api\Schema\Type\InputObject\GeoDistanceType;
use Youshido\GraphQL\Field\AbstractField;
use Youshido\GraphQL\Type\AbstractType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;

class GeoDistanceField extends AbstractField
{
    /**
     * @return AbstractObjectType|AbstractType
     */
    public function getType()
    {
        return new GeoDistanceType();
    }

    public function getName()
    {
        return '_geo_distance';
    }
}
