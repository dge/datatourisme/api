<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api;

use Datatourisme\Api\Processor\ProcessorBuilder;
use Datatourisme\Api\Resolver\ResolverInterface;

class DatatourismeApiBuilder
{
    /** @var ResolverInterface */
    protected $resolver;

    /** @var string */
    protected $schema;

    /** @var mixed */
    protected $cache;

    /** @var array */
    protected $entryPoints;

    /**
     * DatatourismeApiBuilder constructor.
     */
    public function __construct()
    {
        $this->schema = dirname(__FILE__).'/../schema/datatourisme.yml';
        $this->entryPoints = [
            'poi' => ':PointOfInterest',
        ];
    }

    /**
     * @param ResolverInterface $resolver
     *
     * @return DatatourismeApiBuilder
     */
    public function setResolver(ResolverInterface $resolver)
    {
        $this->resolver = $resolver;

        return $this;
    }

    /**
     * @param string $schema
     *
     * @return DatatourismeApiBuilder
     */
    public function setSchema(string $schema)
    {
        $this->schema = $schema;

        return $this;
    }

    /**
     * @param $cache
     *
     * @return DatatourismeApiBuilder
     */
    public function setCache($cache)
    {
        $this->cache = $cache;

        return $this;
    }

    /**
     * @param array $entryPoints
     *
     * @return DatatourismeApiBuilder
     */
    public function setEntryPoints(array $entryPoints)
    {
        $this->entryPoints = $entryPoints;

        return $this;
    }

    /**
     * @param $name
     * @param $type
     *
     * @return DatatourismeApiBuilder
     */
    public function addEntryPoint($name, $type)
    {
        $this->entryPoints[$name] = $type;

        return $this;
    }

    /**
     * @return DatatourismeApi
     */
    public function build()
    {
        $builder = new ProcessorBuilder($this->resolver, $this->schema);
        if ($this->cache) {
            $builder->getLoader()->setCache($this->cache);
        }

        foreach ($this->entryPoints as $name => $type) {
            $builder->addEntrypoint($name, $type);
        }

        return new DatatourismeApi($builder->getProcessor());
    }
}
